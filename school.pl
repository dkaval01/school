% Author - Domantas Kavaliauskas

% Teachers
teacher(knight).
teacher(gross).
teacher(mcevoy).
teacher(appleton).
teacher(parnell).

% Woman out of teacher's
woman(gross).
woman(appleton).
woman(parnell).

% Subject
subject(maths).
subject(science).
subject(history).
subject(english).
subject(pe).

% County
county(suffolk).
county(cornwall).
county(norfolk).
county(yorkshire).
county(cumbria).
county(hertfordshire).

% Activity
activity(nudist-colony).
activity(body-boarding).
activity(swimming).
activity(sightseeing).
activity(camping).


solve :-
	% Subject of each teacher
	subject(KnightSubject), subject(GrossSubject), subject(McEvoySubject), subject(AppletonSubject), subject(ParnellSubject),
	all_different([KnightSubject, GrossSubject, McEvoySubject, AppletonSubject, ParnellSubject]),

	% County of each teacher
	county(KnightCounty), county(GrossCounty), county(McEvoyCounty), county(AppletonCounty), county(ParnellCounty),
	all_different([KnightCounty, GrossCounty, McEvoyCounty, AppletonCounty, ParnellCounty]),

	% Activity of each teacher
	activity(KnightActivity), activity(GrossActivity), activity(McEvoyActivity), activity(AppletonActivity), activity(ParnellActivity),
	all_different([KnightActivity, GrossActivity, McEvoyActivity, AppletonActivity, ParnellActivity]),


	Quadriples = [[knight, KnightSubject, KnightCounty, KnightActivity],
			[gross, GrossSubject, GrossCounty, GrossActivity],
			[mcevoy, McEvoySubject, McEvoyCounty, McEvoyActivity],
			[appleton, AppletonSubject, AppletonCounty, AppletonActivity],
			[parnell, ParnellSubject, ParnellCounty, ParnellActivity]],

	% 1. Ms. Gross teaches either maths or science.
	(   member([gross, maths, _, _], Quadriples);
	    member([gross, science, _, _], Quadriples)	),

	% If Ms. Gross is going to a nudist colony, then she is going to Suffolk;
	% otherwise she is going to Cornwall.
	(     member([gross, _, suffolk, nudist-colony], Quadriples);
	     ( \+(GrossActivity = nudist-colony),
		  member([gross, _, cornwall, _], Quadriples))),


	% 2. The science teacher (who is going body-boarding) is going to
	% travel to Cornwall or Norfolk.
	(   member([_, science, cornwall, body-boarding], Quadriples);
	    member([_, science, norfolk, body-boarding], Quadriples)),


	% 3. Mr. McEvoy (who is the history teacher) is going to Yorkshire or Cumbria.
	McEvoySubject = history,
	(   member([mcevoy, history, yorkshire, _], Quadriples);
	    member([mcevoy, history, cumbria, _], Quadriples)),

	% 4. If the woman who is going to Hertfordshire is the English teacher, then she is Ms. Appleton;
	% otherwise, she is Ms. Parnell (who is going swimming).

	woman(XX),
	(  (member([XX, english, hertfordshire, _], Quadriples),
		XX = appleton,
		\+ (XX = parnell))	;
	   (member([XX, _, hertfordshire, _], Quadriples),
		XX = parnell) ),
	ParnellActivity = swimming,

	% 5. The person who is going to Yorkshire (who isn't the PE teacher) isn't the one who is going sightseeing.
	teacher(NotPE),
	member([NotPE, _, yorkshire, _], Quadriples),
	\+ member([NotPE, pe, _, _], Quadriples),
	\+ member([NotPE, _, _, sightseeing], Quadriples),

	% 6. Ms. Gross isn't the woman who is going camping.
	woman(XY),
	\+ (XY = gross),
	member([XY, _, _, camping], Quadriples),

	% 7. One woman is going to a nudist colony on her holiday.
	woman(XZ),
	member([XZ, _, _, nudist-colony], Quadriples),

	tell(knight, KnightSubject, KnightCounty, KnightActivity),
	tell(gross, GrossSubject, GrossCounty, GrossActivity),
	tell(mcevoy, McEvoySubject, McEvoyCounty, McEvoyActivity),
	tell(appleton, AppletonSubject, AppletonCounty, AppletonActivity),
	tell(parnell, ParnellSubject, ParnellCounty, ParnellActivity).

% Succeeds if all elements of the argument list are bound and different.
% Fails if any elements are unbound or equal to some other element.
all_different([H | T]) :- member(H, T), !, fail.
all_different([_ | T]) :- all_different(T).
all_different([_]).

tell(X, Y, Z, Q) :-
    write(X), write(' teaches '), write(Y), write(' and is going on holiday to '),
    write(Z), write(' to do '), write(Q), write('.'), nl.
